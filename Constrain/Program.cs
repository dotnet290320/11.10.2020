﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericConstr
{
    class Program
    {
        abstract class Shape
        {
            public string Name { get; set; }
            public Shape(string name)
            {
                Name = name;
            }
        }
        class Circle : Shape
        {
            public float Radius { get; set; }
            public Circle(string name, float radius) : base(name)
            {
                Radius = radius;
            }
        }
        class Triangle : Shape
        {
            public float A { get; set; }
            public float B { get; set; }
            public float C { get; set; }
            public Triangle(string name, float a, float b, float c) : base(name)
            {
                A = a;
                B = b;
                C = c;
            }
        }
        class Bag { }

        class MyToolTip<T> where T : Shape
        {
            private T m_shape;
            public MyToolTip(T shape)
            {
                m_shape = shape;
            }
            public string GetToolTip()
            {
                return $"data ..... {m_shape.Name}";
            }
        }

        static void Main(string[] args)
        {
            Circle c = new Circle("nice circle", 7.1f);
            Shape s = c;
            MyToolTip<Circle> tool = new MyToolTip<Circle>(c);
            // MyToolTip<Bag> tool1 = new MyToolTip<Bag>(new Bag()); // ERROR! Bag not inherit from Shape

        }
    }
}
