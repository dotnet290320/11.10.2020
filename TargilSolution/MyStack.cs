﻿using System;
using System.Collections.Generic;

namespace GenericSolution
{
    class MyStack<T> : MyStacker<T>
    {
        protected List<T> m_list = new List<T>();
        public T FirstItem
        {
            get
            {
                return m_list[0];
            }
        }
        public void Clear()
        {
            m_list.Clear();
        }

        public virtual T Peep()
        {
            if (m_list.Count == 0)
                throw new IndexOutOfRangeException("Empty stack...");

            return m_list[0];
        }

        public T Pop()
        {
            if (m_list.Count == 0)
                throw new IndexOutOfRangeException("Empty stack...");

            T result = m_list[0];
            m_list.RemoveAt(0);
            return result;
        }

        public void Push(T item)
        {
            m_list.Insert(0, item);
        }
    }
}
