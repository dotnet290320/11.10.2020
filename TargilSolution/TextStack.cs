﻿namespace GenericSolution
{
    class TextStack : MyStack<string>
    {
        public override string Peep()
        {
            if (m_list.Count == 0)
                return "nothing...";

            return m_list[0];
        }
        public string PrintAll()
        {
            string result = "";
            m_list.ForEach(item => result = result + item + ".");
            return result;
        }
    }
}
