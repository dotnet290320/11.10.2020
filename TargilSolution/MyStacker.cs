﻿namespace GenericSolution
{
    interface MyStacker<T>
    {
        T Peep();
        void Push(T item);
        void Clear();
        T Pop();
        T FirstItem { get; }
    }
}
