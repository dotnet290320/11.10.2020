﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            IntDataPrinter data = new IntDataPrinter(10);
            data.Print();
            DoubleDataPrinter data_d = new DoubleDataPrinter(283.283747);
            data_d.Print();

            // type sef
            // no boxing unboxing
            // no code duplications
            // no usage of objects!
            // fun ...
            
            // bad practice
            //GenericDataPrint<object> data_printer_total_price = new GenericDataPrint<object>(1900.5); // badddd
            //double price = (double)data_printer_total_price.Data; //unboxing
            //data_printer_total_price.Data = 10; // boxing
            //data_printer_total_price.Data = "hello world!"; // not type safe

            GenericDataPrint<double> data_printer_total_price = new GenericDataPrint<double>(1900.5);
            double price = data_printer_total_price.Data;
            data_printer_total_price.Data = 10;
            //data_printer_total_price.Data = "hello world!";

            GenericDataPrint<int> data_printer_integer = new GenericDataPrint<int>(10);
            GenericDataPrint<int> data_printer_integer2 = new GenericDataPrint<int>(10);

            System.Console.WriteLine(GenericDataPrint<int>.counter); // 2
            System.Console.WriteLine(GenericDataPrint<double>.counter); // 1
            System.Console.WriteLine(GenericDataPrint<object>.counter); // 0

            //Dictionary<string, int> map_name_to_id = new Dictionary<string, int>();
            //int id = map_name_to_id[2];

            System.Console.WriteLine(data_printer_total_price.GetData());

        }
    }
}
