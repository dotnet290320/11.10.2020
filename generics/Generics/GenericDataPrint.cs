﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Generics
{
    interface IDataGetter<T>
    {
        T GetData();
    }

    class MyGetterOfData : IDataGetter<double>
    {
        public double GetData()
        {
            throw new NotImplementedException();
        }
    }
    abstract class Papa<T>
    {
        public T PapaData { get; set; }
    }
    // note: generic class can be abstract
    class GenericDataPrint<T> : Papa<double>, IDataGetter<T>
    {
        protected T m_data; // member
        private readonly DateTime m_creation_time;
        public static int counter = 0;
        public static T static_data;

        protected List<T> m_list; // list of T
        public T GetData()
        {
            throw new NotImplementedException();
        }

        public T Data // property
        {
            get
            {
                return m_data;
            }
            set // T value
            {
                m_data = value;
            }
        }

        public GenericDataPrint(T data) // ctor - func arg
        {
            counter++;
            m_data = data;
            m_list = new List<T>();
            m_creation_time = DateTime.Now;
        }

        // operator overloading supported ... can you use it ?
        //public static T operator+(GenericDataPrint<T> t1, GenericDataPrint<T> t2)
        //{
        //    return t1.Data + t2.Data;
        //}
        public static bool operator==(GenericDataPrint<T> t1, GenericDataPrint<T> t2)
        {
            return t1.Data.Equals(t2.Data);
        }
        public static bool operator !=(GenericDataPrint<T> t1, GenericDataPrint<T> t2)
        {
            return !(t1 == t2);
        }

        // indexer
        public T this[int index]
        {
            get
            {
                return m_list[index];
            }
        }

        public virtual void Print() // func
        {
            Console.WriteLine($"!============== {m_data} ================");
        }
    }

    class FloatDataPrint : GenericDataPrint<float>
    {
        public FloatDataPrint(float data) : base(data)
        {

        }

        public override void Print()
        {
            Console.WriteLine($"!============== {String.Format("{0:0.00}", m_data)} ================");
        }
    }
}
