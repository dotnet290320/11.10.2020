﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class IntDataPrinter
    {
        private int m_data;

        public IntDataPrinter(int data)
        {
            m_data = data;
        }
        public void Print()
        {
            Console.WriteLine($"!============== {m_data} ================");
        }
    }
}
