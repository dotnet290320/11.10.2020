﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class DoubleDataPrinter
    {
        private double m_data;

        public DoubleDataPrinter(double data)
        {
            m_data = data;
        }
        public void Print()
        {
            Console.WriteLine($"!============== {String.Format("{0:0.00}", m_data)} ================");
        }
    }
}
