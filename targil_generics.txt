

write a generic class of a stack:
MyStack<T>
List<T>
T FirstItem { getter ...  } -- return the first item in stack (without removing), only if exists
Peep
Push
Pop
Clear
Indexer [ int index ] -- show the item with index 

interface MyStacker<T>
Peep, Push, Pop, Clear

create a derived TextStack : MyStack<string>
Peep -- if no item exist -- return "nothing"
PrintAll () -- concatenate all items in stack 
